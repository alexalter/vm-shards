# vm-kube-monitoring

## Install

### Install operator:
```
helm install vm-operator victoria-metrics-operator
```
### Remove default metrics server apiservice:
```
kubectl delete apiservices.apiregistration.k8s.io v1beta1.metrics.k8s.io
```
### Install vm monitoring
```
helm install vm-monitoring vm-cluster-monitoring
```
### Install with custom shard count(default 2):
```
helm upgrade vm-monitoring vm-cluster-monitoring --set vmagent.shardCount=3
```

## Restore metrics server apiservice
```
kubectl apply -f metrics-apiservice.yaml
```
